# Travel API
## Introduction
Travel API Client is used for the calculation fares between two selected airports. 

The request from the Client is handled in the backend service and make OAuth2 authentication from the Mock Service which provide airport information and fare calculation.

## Architecture
The Microservice architecture is used for this application. 

There are mainly 4 different services behind.
- **Airports Service**
- **Fare Service**
- **Metrics Service**
- **Frontend Service**

![Architecture](https://bytebucket.org/cengizyilmaz/klm-it-case/raw/ca53f10376ea72b597e5ae1bfd632a74f6fa4d47/Nodes.png)

Mock service behind these services are provided the airports data and fare information through its REST API.

Mock service is protected via OAuth2 authentication. Every request from Airports and Fare Service provide the Authentication Token  to Mock Service.

The Web Client  of the Application is written with AngularJS.  The Web Client is sends its request to the Services via WebSocket connection. The Web Client is connected to Service via following WebSocket Endpoints.
- Airports Service 
	- **/airports**: This endpoint of websocket is used for getting the airports information from mock service.
- Fare Service
	- **/fare**: This endpoint of websocket is used for calculating fare between two airports from the mock service. Fare calculation is done through Reactive extension of Jersey to Mock Service. So there is a reactive REST interface between two services.
- Metric Service
	- **/display**: This endpoint of websocket is used to display the metric information

The application provides the **Http Status Metrics** and **Execution Time Metrics**.
The Metrics are gathered from Airports Service, Fare Service and Metric Service. 

Metrics Service and other 2 services (Airports and Fare Service) are connected via websocket.  When the Services initialize **/collect** websocket endpoint initialize between Metrics Service and 2  services.

Execution Time Metrics is calculated through the Execution Time Metric aspect. The aspect pointcut is defined in the **@ExecutionTimeMetric** annotation and executed around REST API invocation to Mock service.
### Technologies
The Services is used following Technologies
- Java 1.8
- Spring Boot
- WebSocket
- Jersey with RX extension
- Microservice

The Client is used following Technologies
- AngularJS
- WebSocket
- Javascript

## Running the Case
In order to run the case, the services should be started first. There is a batch command to execute the service run.
### Windows
Start with  

    cmd bootRun-all.bat
  
### Linux
Start with

    bootRun-all_Linux.sh

The command will executed the Boot run for each service in a following sequence 
- Travel-Api-Mock-Service
- Airport Service
- Fare Service
- FrontEnd Service
- Metric Service

If all of the service started successfully, The Web Client is displayed following URL.

[http://localhost:9000](http://localhost:9000)
   
Demo video is in the following link

[Demo Video](https://bitbucket.org/cengizyilmaz/klm-it-case/raw/ca53f10376ea72b597e5ae1bfd632a74f6fa4d47/demo_klm_it_case.mp4)

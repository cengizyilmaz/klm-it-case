package com.afkl.cases.df.airports.dto;

/**
 * This class is the Data Transfer Object class to store Coordinate of Airport
 * which requested from mock service.
 *
 * @author Cengiz YILMAZ
 * 
 */
public class Coordinates {
	private double latitude;
	private double longitude;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}

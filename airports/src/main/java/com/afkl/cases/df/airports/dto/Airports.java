package com.afkl.cases.df.airports.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is the Data Transfer Object class to return the Airports request
 * from mock service. This class contains the list of the airports from the
 * request.
 *
 * @author Cengiz YILMAZ
 * 
 */
public class Airports {

	@JsonProperty(value = "locations")
	private Set<Airport> airportList = new HashSet<>();

	@JsonProperty(value = "request_id")
	private Integer requestId;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public Set<Airport> getAirportList() {
		return airportList;
	}

	public void setAirportList(Set<Airport> airportList) {
		this.airportList = airportList;
	}

}

package com.afkl.cases.df.airports.services;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import com.afkl.cases.df.util.config.properties.ConfigProperties;
import com.afkl.cases.df.util.helper.ServerStatus;
import com.afkl.cases.df.util.metrics.service.MetricsAgentWsHandler;

/**
 * This class is the initialization Service Bean. This class is used to make
 * connection to Metric Service. If the metric service is started before the
 * airports service, this class makes websocket connection.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Service
public class InitializationService {

	@Autowired
	private MetricsAgentWsHandler handler;
	@Autowired
	private ConfigProperties configProp;

	@PostConstruct
	public void init() {
		String host = configProp.getMetrics().getHost();
		Integer port = configProp.getMetrics().getPort();
		if (!ServerStatus.serverListening(port)) {
			StandardWebSocketClient client = new StandardWebSocketClient();
			StringBuilder wsUri = new StringBuilder();
			wsUri.append("ws://").append(host).append(":").append(port).append("/collect");
			WebSocketConnectionManager manager = new WebSocketConnectionManager(client, handler, wsUri.toString());
			manager.setAutoStartup(true);
			manager.start();
		}
	}

	@Bean
	public StandardWebSocketClient client() {
		return new StandardWebSocketClient();
	}

}

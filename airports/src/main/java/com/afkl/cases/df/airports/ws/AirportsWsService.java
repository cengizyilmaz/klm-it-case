package com.afkl.cases.df.airports.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.afkl.cases.df.airports.dto.Airports;
import com.afkl.cases.df.airports.services.AirportsService;
import com.afkl.cases.df.util.metrics.ExecutionTimeMetric;
import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket Airports service.This websocket service class is
 * used to operation from websocket handler and return message through if
 * necessary
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class AirportsWsService {

	@Autowired
	private AirportsService airportsService;

	@Autowired
	private SessionManager<WebSocketSession> sessionManager;

	private String id;

	/**
	 * This method is used to send airports websocket request to service class and
	 * response to client through websocket session
	 * 
	 * @param sessionId
	 * @param code
	 * @param lang
	 * @param requestId
	 */
	@ExecutionTimeMetric
	public void getAirports(String sessionId, String code, String lang, Integer requestId) {
		Airports airports = airportsService.getAirports(code, lang);
		airports.setRequestId(requestId);
		sessionManager.sendMessage(airports, sessionId, AirportsWsHandler.class);
	}

	@ExecutionTimeMetric
	public void listAirports(String sessionId,String page,String count,String lang,Integer requestId)
	{
		Airports airports = airportsService.listAirports(page,count, lang);
		airports.setRequestId(requestId);
		sessionManager.sendMessage(airports, sessionId, AirportsWsHandler.class);
	}
	public String getId() {
		return id;
	}

	public void setId(String _id) {
		id = _id;
	}

}

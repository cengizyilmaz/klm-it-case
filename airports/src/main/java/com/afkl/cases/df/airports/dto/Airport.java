package com.afkl.cases.df.airports.dto;
/**
 * This class is the Data Transfer Object class to return the Airports request from mock service.
 * This class contains the specific airport information by itself.
 *
 * @author Cengiz YILMAZ
 * 
 */
public class Airport {
	private String code;
	
	private String name;
	
	private String description;
	
	private Coordinates coordinates;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	

}

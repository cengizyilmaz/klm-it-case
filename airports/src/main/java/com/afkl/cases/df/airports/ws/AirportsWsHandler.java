package com.afkl.cases.df.airports.ws;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.afkl.cases.df.util.sessionmanager.SessionManager;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is the WebSocket handler for Airports service.This handler is to
 * observe the websocket state. Connection established, closed and message
 * handling are handled by this class
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class AirportsWsHandler extends TextWebSocketHandler {

	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private AirportsWsService airportWsService;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionManager.addSession(session, session.getId(), this.getClass());

		super.afterConnectionEstablished(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessionManager.removeSession(session.getId(), this.getClass());
		System.out.println("Airports gathering Session is closed");
		super.afterConnectionClosed(session, status);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings("unchecked")
		Map<String, Object> request = mapper.readValue(message.getPayload().toString(), Map.class);
		Integer requestId = Integer.valueOf(request.get("request_id").toString());
		String messageType = request.get("messageType").toString();
		if (messageType.equals("getAirports")) {
			String code = request.get("data").toString();
			String lang = request.get("lang").toString();
			airportWsService.getAirports(session.getId(), code, lang, requestId);
		}
		else if(messageType.equals("listAirports"))
		{
			String lang = request.get("lang").toString();
			String page=request.get("page").toString();
			String count=request.get("count").toString();
					
			airportWsService.listAirports(session.getId(), page,count, lang, requestId);
		}

		super.handleMessage(session, message);
	}
}

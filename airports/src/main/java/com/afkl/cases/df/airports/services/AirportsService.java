package com.afkl.cases.df.airports.services;

import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.airports.dto.Airport;
import com.afkl.cases.df.airports.dto.Airports;
import com.afkl.cases.df.airports.dto.AirportsResult;
import com.afkl.cases.df.oauth.OAuthService;
import com.afkl.cases.df.util.config.properties.ConfigProperties;
/**
 * Airports Service is used to get the airports for the specific Airport code.
 * This Service makes a Rest connection to backend mockup service.
 * Before making connection, the OAuth2 token is gathered from mockup service.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Service
public class AirportsService {

	@Autowired
	private ConfigProperties configManager;

	@Autowired 
	private OAuthService oauthService;
	
	@Autowired
	private OAuth2RestOperations restTemplate;
	
	@Autowired
	private AirportSortingComparator sortingComparator;
	
	/**
	 * This method is called to get the airports according to code. 
	 * The method is called from the UI websocket and called this method.
	 * The Rest connection is done for getting the airports.
	 * @param code
	 * @return
	 */
	public Airports getAirports(String code,String lang)
	{
		String accessToken= oauthService.getToken();
		String langParam=lang!=null?"&lang="+lang:"";
		 String url=configManager.getUrl()+configManager.getAirportsList()+ "?term=" +code
	             +langParam  
				 + "&access_token=" + accessToken;
		
		 AirportsResult airports= restTemplate.getForObject(url,AirportsResult.class);
		 if(airports!=null && airports.getAirports()!=null && airports.getAirports().getAirportList()!=null && !airports.getAirports().getAirportList().isEmpty())
		 {
			Set<Airport> sortedList=new TreeSet<>(sortingComparator);
			sortedList.addAll(airports.getAirports().getAirportList());
			 airports.getAirports().setAirportList(sortedList);
		 }
		return airports.getAirports();
	}
	
	public Airports listAirports(String page,String count,String lang)
	{
		String accessToken= oauthService.getToken();
		String langParam=lang!=null?"&lang="+lang:"";
		 String url=configManager.getUrl()+configManager.getAirportsList()+"?"
	             +langParam  
	             +"&page="+page
	             +"&size="+count
				 + "&access_token=" + accessToken;
		
		 AirportsResult airports= restTemplate.getForObject(url,AirportsResult.class);
		 if(airports!=null && airports.getAirports()!=null && airports.getAirports().getAirportList()!=null && !airports.getAirports().getAirportList().isEmpty())
		 {
			Set<Airport> sortedList=new TreeSet<>(sortingComparator);
			sortedList.addAll(airports.getAirports().getAirportList());
			 airports.getAirports().setAirportList(sortedList);
		 }
		 return airports.getAirports();
	}

}

package com.afkl.cases.df.airports.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is the Data Transfer Object class to return the Airports request
 * from mock service. This class wrap the response from the mock service only.
 *
 * @author Cengiz YILMAZ
 * 
 */
public class AirportsResult {

	@JsonProperty(value = "_embedded")
	private Airports airports;

	public Airports getAirports() {
		return airports;
	}

	public void setAirports(Airports airports) {
		this.airports = airports;
	}

}

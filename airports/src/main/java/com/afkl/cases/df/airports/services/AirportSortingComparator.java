package com.afkl.cases.df.airports.services;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import com.afkl.cases.df.airports.dto.Airport;

@Component
public class AirportSortingComparator implements Comparator<Airport> {

	@Override
	public int compare(Airport arg0, Airport arg1) {
		return arg0.getName().compareTo(arg1.getName());
	
	}

}

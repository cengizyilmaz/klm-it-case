package com.afkl.cases.df.airports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan(basePackages= {"com.afkl.cases.df"})
@SpringBootApplication
public class AirportsApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		SpringApplication.run(AirportsApplication.class, args);
	}
	
}

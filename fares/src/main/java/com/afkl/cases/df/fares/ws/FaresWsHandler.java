package com.afkl.cases.df.fares.ws;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.afkl.cases.df.util.sessionmanager.SessionManager;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is the WebSocket handler for Fare service.This handler is to
 * observe the websocket state. Connection established, closed and message
 * handling are handled by this class.The /fare endpoint handled in this handler
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class FaresWsHandler extends TextWebSocketHandler {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private FaresWsService faresWsService;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionManager.addSession(session, session.getId(), this.getClass());

		super.afterConnectionEstablished(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessionManager.removeSession(session.getId(), this.getClass());
		System.out.println("Fare Calculation Session is closed");
		super.afterConnectionClosed(session, status);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> request = mapper.readValue(message.getPayload().toString(), Map.class);
		String from = request.get("from").toString();
		String to = request.get("to").toString();
		Integer requestId = Integer.valueOf(request.get("request_id").toString());
		faresWsService.calculate(from, to, session.getId(), requestId);

		super.handleMessage(session, message);
	}
}

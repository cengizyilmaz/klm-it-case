package com.afkl.cases.df.fares.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.afkl.cases.df.fares.ws.FaresWsHandler;
import com.afkl.cases.df.util.metrics.service.MetricsAgentWsHandler;;

/**
 * This class is the Web Socket Configuration class. This class is used to
 * register handlers for the specific uri.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Configuration
@EnableWebSocket
public class FaresWsConfig implements WebSocketConfigurer {
	@Autowired
	private FaresWsHandler handler;
	@Autowired
	private MetricsAgentWsHandler metricHandler;
	/**
	 * This method is used the register the websocket endpoint to Handler. There are
	 * 2 different endpoint for websocket; /fares and /collect
	 *
	 */
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(handler, "/fares").setAllowedOrigins("*");
		registry.addHandler(metricHandler, "/collect").setAllowedOrigins("*");

	}

}

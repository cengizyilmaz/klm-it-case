package com.afkl.cases.df.fares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan(basePackages= {"com.afkl.cases.df"})
@SpringBootApplication
public class FaresApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaresApplication.class, args);
	}

}

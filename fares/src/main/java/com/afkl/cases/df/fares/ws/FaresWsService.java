package com.afkl.cases.df.fares.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.afkl.cases.df.fares.model.Fare;
import com.afkl.cases.df.fares.service.FaresService;
import com.afkl.cases.df.util.metrics.ExecutionTimeMetric;
import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket Fare service.This websocket service class is used
 * to operation from websocket handler and return message through if necessary
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class FaresWsService {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private FaresService fareService;

	/**
	 * This method is used to handle fare calculation websocket request,send to
	 * service object and response to client through websocket session
	 * 
	 * @param fromCode
	 *            Departure Airport code
	 * @param toCode
	 *            Destination Airport code
	 * @param sessionId
	 *            Session ID for websocket session
	 * @param requestId
	 *            Request ID for client request
	 */
	@ExecutionTimeMetric
	public void calculate(String fromCode, String toCode, String sessionId, Integer requestId) {

		Fare fare = fareService.calculate(fromCode, toCode);
		if (fare != null) {
			fare.setRequestId(requestId);
			sessionManager.sendMessage(fare, sessionId, FaresWsHandler.class);
		}

	}
}

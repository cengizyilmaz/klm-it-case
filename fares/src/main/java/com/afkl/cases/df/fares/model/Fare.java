package com.afkl.cases.df.fares.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is the Data Transfer Object class to return the Fare request from
 * mock service.
 * 
 *
 * @author Cengiz YILMAZ
 * 
 */
public class Fare {

	private double amount;

	private FareCurrency currency;

	private String origin;

	private String destination;
	@JsonProperty(value = "request_id")
	private Integer requestId;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double _amount) {
		this.amount = _amount;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public FareCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(FareCurrency currency) {
		this.currency = currency;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}

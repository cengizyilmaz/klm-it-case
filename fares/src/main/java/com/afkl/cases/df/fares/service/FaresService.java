package com.afkl.cases.df.fares.service;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.rx.RxClient;
import org.glassfish.jersey.client.rx.RxInvocationBuilder;
import org.glassfish.jersey.client.rx.RxWebTarget;
import org.glassfish.jersey.client.rx.java8.RxCompletionStage;
import org.glassfish.jersey.client.rx.java8.RxCompletionStageInvoker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.fares.model.Fare;
import com.afkl.cases.df.oauth.OAuthService;
import com.afkl.cases.df.util.config.properties.ConfigProperties;

/**
 * Fare Service is used to get the fare calculation for the departure and
 * destination airports This Service makes a Rest connection to backend mockup
 * service. Before making connection, the OAuth2 token is gathered from mockup
 * service.
 * 
 *
 * @author Cengiz YILMAZ
 * 
 */
@Service
public class FaresService {
	@Autowired
	private ConfigProperties configManager;
	@Autowired
	private OAuthService oauthService;

	/**
	 * This method is used to get the calculation for Fare between destination and
	 * departure airports. The Rx extension for jersey is used to reactive rest
	 * connection.
	 * The method is called from the UI websocket and called this method.
	 * 
	 * @param from
	 *            Departure Airport code
	 * @param to
	 *            Destination Airport code
	 * @return Fare object for the calculation
	 */
	public Fare calculate(String from, String to) {
		String accessToken = oauthService.getToken();

		Fare fare = null;
		Response rest = null;
		try {
			RxClient<RxCompletionStageInvoker> client = RxCompletionStage.newClient();
			RxCompletionStageInvoker invoker = null;
			if (client != null) {
				RxWebTarget<RxCompletionStageInvoker> target = client.target(configManager.getUrl());
				if (target != null) {
					RxInvocationBuilder<RxCompletionStageInvoker> builder = target
							.path(configManager.getFareCalculate()).path(from).path(to)
							.queryParam("access_token", accessToken).request();
					if (builder != null) {
						invoker = builder.rx();
					}
				}
				CompletionStage<Response> stage = invoker.get();

				rest = stage.toCompletableFuture().get();
				int status = rest.getStatus();

				if (status == 200) {
					fare = rest.readEntity(Fare.class);

				}

			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		} finally {
			if (rest != null) {
				rest.close();
			}
		}

		return fare;
	}
}

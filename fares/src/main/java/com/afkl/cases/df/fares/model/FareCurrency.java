package com.afkl.cases.df.fares.model;

/**
 * This Enumaration is to used for Fare currency about the fare calculation
 * There are 2 values: EUR and USD
 * 
 *
 * @author Cengiz YILMAZ
 * 
 */
public enum FareCurrency {
	EUR("EUR"), USD("USD");
	private String value;

	FareCurrency(final String newValue) {
		value = newValue;
	}

	public String getValue() {
		return value;
	}

}

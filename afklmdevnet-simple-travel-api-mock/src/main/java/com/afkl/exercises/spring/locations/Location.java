package com.afkl.exercises.spring.locations;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

import lombok.Value;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
//@Value
public class Location {

    private String code, name, description;
    private Coordinates coordinates;
    private Location parent;
    private Set<Location> children;
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	public String getCode() {
		// TODO Auto-generated method stub
		return code;
	}
	public String getDescription() {
		// TODO Auto-generated method stub
		return description;
	}
	public Coordinates getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

}

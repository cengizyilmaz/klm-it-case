package com.afkl.exercises.spring.fares;


public class Fare {

   
	private double amount;
    private Currency currency;
    private String origin, destination;
	public Fare(double doubleValue, Currency valueOf, String code, String code2) {
		amount=doubleValue;
		currency=valueOf;
		origin=code;
		destination=code2;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
    
    

}

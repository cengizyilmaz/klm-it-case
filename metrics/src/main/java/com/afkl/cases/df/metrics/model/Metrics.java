package com.afkl.cases.df.metrics.model;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * This class is the Data Transfer Object class to return the Fare request from
 * mock service.
 * 
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class Metrics {

	private List<String> labels = new LinkedList<>();

	private List<Object> data = new LinkedList<>();

	private double average;
	private Long min;
	private Long max;

	@JsonProperty(value = "request_id")
	private Integer requestId;

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public Long getMin() {
		return min;
	}

	public void setMin(Long min) {
		this.min = min;
	}

	public Long getMax() {
		return max;
	}

	public void setMax(Long max) {
		this.max = max;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}
}

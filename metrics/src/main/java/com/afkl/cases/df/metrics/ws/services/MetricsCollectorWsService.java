package com.afkl.cases.df.metrics.ws.services;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.afkl.cases.df.metrics.model.Metrics;
import com.afkl.cases.df.metrics.services.MetricsService;
import com.afkl.cases.df.metrics.ws.MetricsWsHandler;
import com.afkl.cases.df.util.metrics.ExecutionMetricGroup;
import com.afkl.cases.df.util.metrics.HttpStatusMetricGroup;
import com.afkl.cases.df.util.metrics.MetricCollector;
import com.afkl.cases.df.util.metrics.MetricGroupType;
import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket Metric Collector service.This websocket service
 * class is used to operation from websocket handler and return message through
 * if necessary. The metric collection is gathered from this service.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class MetricsCollectorWsService {

	@Autowired
	private MetricCollector collector;
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private Metrics metrics;
	@Autowired
	private MetricsService service;

	public void collectMetric(MetricCollector collected) {
		Set<Entry<String, Object>> result = service.invoke();
		HttpStatusMetricGroup httpStatGroup = collector.getHttpStatusMetricGroup();
		httpStatGroup.mergeMetrics(result);
		collector.mergeGroupList(collected);
		ExecutionMetricGroup group = (ExecutionMetricGroup) collector
				.getMetricGroup(MetricGroupType.EXECUTIONMETRIC.name());
		if (!group.getMetrics().isEmpty()) {
			double average = group.getMetrics().stream().mapToDouble(Long::doubleValue).average().getAsDouble();
			Long max = group.getMetrics().stream().max(Comparator.naturalOrder()).get();
			Long min = group.getMetrics().stream().min(Comparator.naturalOrder()).get();

			metrics.setAverage(average);
			metrics.setMax(max);
			metrics.setMin(min);
		}
		HttpStatusMetricGroup statusMetric = (HttpStatusMetricGroup) collector
				.getMetricGroup(MetricGroupType.HTTPSTATUS.name());
		List<String> labels = statusMetric.getStatusList().entrySet().stream().map(Map.Entry::getKey)
				.collect(Collectors.toList());
		metrics.setLabels(labels);
		List<Object> data = statusMetric.getStatusList().entrySet().stream().map(Map.Entry::getValue)
				.collect(Collectors.toList());
		metrics.setData(data);
		metrics.setRequestId(0);
		sessionManager.sendMessage(metrics, MetricsWsHandler.class);

		;
	}
}

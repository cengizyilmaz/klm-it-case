package com.afkl.cases.df.metrics.services;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import com.afkl.cases.df.metrics.ws.MetricsCollectorWsHandler;
import com.afkl.cases.df.util.config.properties.ConfigProperties;
import com.afkl.cases.df.util.config.properties.MetricsProperties;
import com.afkl.cases.df.util.helper.ServerStatus;

/**
 * This class is the initialization Service Bean. This class is used to make
 * connection to Other Rest Services(airports and fare services). If the other
 * service is started before the metric service, this class makes websocket
 * connection.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Service
public class InitializationService {

	@Autowired
	private MetricsCollectorWsHandler handler;
	@Autowired
	private ConfigProperties configProp;

	@PostConstruct
	public void init() {
		List<MetricsProperties> metricList = configProp.getMetrics().getCollectors();
		for (MetricsProperties properties : metricList) {
			String host = properties.getHost();
			Integer port = properties.getPort();
			if (!ServerStatus.serverListening(port)) {
				
				clientConnect(host,port);
			}
		}
	}
	public void clientConnect(String host,Integer port)
	{
		StandardWebSocketClient client = new StandardWebSocketClient();
		StringBuilder wsUri = new StringBuilder();
		wsUri.append("ws://").append(host).append(":").append(port).append("/collect");
		WebSocketConnectionManager manager = new WebSocketConnectionManager(client, handler, wsUri.toString());
		manager.setAutoStartup(true);
		manager.start();
	}
	@Bean
	public StandardWebSocketClient client() {
		return new StandardWebSocketClient();
	}

}

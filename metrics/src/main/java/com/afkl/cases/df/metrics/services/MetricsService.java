package com.afkl.cases.df.metrics.services;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.stereotype.Service;

/**
 * Metric Service is used to get the metrics for this service. This metrics are
 * summe with the other service metrics and resulted to client accordingly.
 * 
 * 
 *
 * @author Cengiz YILMAZ
 * 
 */
@Service
public class MetricsService {
	@Autowired
	private MetricsEndpoint metricsEndPoint;

	public Set<Entry<String, Object>> invoke() {
		Map<String, Object> metriclist = metricsEndPoint.invoke();
		Set<Entry<String, Object>> result = metriclist.entrySet().stream()
				.filter(item -> item.getKey().startsWith("counter.status")).collect(Collectors.toSet());
		return result;

	}

}

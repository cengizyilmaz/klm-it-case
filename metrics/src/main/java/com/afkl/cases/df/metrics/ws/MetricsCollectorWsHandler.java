package com.afkl.cases.df.metrics.ws;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.afkl.cases.df.metrics.services.InitializationService;
import com.afkl.cases.df.metrics.ws.services.MetricsCollectorWsService;
import com.afkl.cases.df.util.metrics.MetricCollector;
import com.afkl.cases.df.util.sessionmanager.SessionManager;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is the WebSocket handler for Metric Collection from other
 * services.This handler is to observe the websocket state. Connection
 * established, closed and message handling are handled by this class.The
 * /collect endpoint handled in this handler
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class MetricsCollectorWsHandler extends TextWebSocketHandler {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private InitializationService initializationService;
	@Autowired
	private MetricsCollectorWsService collectorService;
	
	private AtomicInteger reconnectCount=new AtomicInteger(0);

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionManager.addSession(session, session.getId(), this.getClass());
		reconnectCount.set(0);
		super.afterConnectionEstablished(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessionManager.removeSession(session.getId(), this.getClass());
		if(CloseStatus.NORMAL!=status && reconnectCount.incrementAndGet()<3)
		{
			initializationService.clientConnect(session.getLocalAddress().getHostName(), session.getLocalAddress().getPort());
		}
		System.out.println("Metric Collector Session is closed. Close Status" + status);
		super.afterConnectionClosed(session, status);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		MetricCollector collector = mapper.readValue(message.getPayload().toString(), MetricCollector.class);
		collectorService.collectMetric(collector);

		super.handleMessage(session, message);
	}
}

package com.afkl.cases.df.metrics.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

/**
 * This class is the Web Socket Configuration class. This class is used to
 * register handlers for the specific uri.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Configuration
@EnableWebSocket
public class MetricsWsConfig implements WebSocketConfigurer {

	@Autowired
	private MetricsWsHandler handler;
	@Autowired
	private MetricsCollectorWsHandler collectorHandler;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(handler, "/display").setAllowedOrigins("*")
				.addInterceptors(new HttpSessionHandshakeInterceptor());
		;
		registry.addHandler(collectorHandler, "/collect").setAllowedOrigins("*");

	}

}

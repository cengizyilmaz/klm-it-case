package com.afkl.cases.df.metrics.ws.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.afkl.cases.df.metrics.ws.MetricsCollectorWsHandler;
import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket Metric service.This websocket service class is
 * used to operation from websocket handler and return message through if
 * necessary. Client web socket request is handled this service and return the
 * metric results.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class MetricsWsService {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;

	public void invoke(Integer requestId, String sessionId) {
		String collectMessage = "";
		sessionManager.sendMessage(collectMessage, MetricsCollectorWsHandler.class);

	}
}

package com.afkl.cases.df.oauth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import com.afkl.cases.df.util.config.oauth2.OAuthConfig;

/**
 * This class is configuration class for OAuth2 which is used for the services
 * needs OAuth.
 * 
 * @author Cengiz YILMAZ
 *
 */
@EnableOAuth2Client
@Configuration
public class OAuthConfigHandler {

	@Autowired
	private OAuthConfig oauthConfig;

	@Bean
	protected OAuth2ProtectedResourceDetails resource() {

		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();

		List<String> scopes = new ArrayList<String>(2);

		scopes.add("read");

		resource.setAccessTokenUri(oauthConfig.getAccessTokenUri());
		resource.setClientId(oauthConfig.getClientID());
		resource.setClientSecret(oauthConfig.getClientSecret());
		resource.setGrantType("client_credentials");
		resource.setScope(scopes);

		return resource;
	}

	@Bean
	public OAuth2RestOperations restTemplate() {
		AccessTokenRequest atr = new DefaultAccessTokenRequest();

		return new OAuth2RestTemplate(resource(), new DefaultOAuth2ClientContext(atr));
	}
}

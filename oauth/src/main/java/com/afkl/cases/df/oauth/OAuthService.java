package com.afkl.cases.df.oauth;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

/**
 * The OAuth token for the mock service is provided from this Service. This
 * class makes REST Request for the Mock Service to get the token.
 * 
 * @author Cengiz YILMAZ
 *
 */
@Service
public class OAuthService {
	@Autowired
	private OAuth2RestOperations restTemplate;

	private String accessToken = null;
	private Date expireDate = null;

	public String getToken() {
		Date currentDate = new Date();
		if (accessToken != null && expireDate != null && currentDate.after(expireDate)) {
			accessToken = null;
		}
		if (accessToken == null) {
			OAuth2AccessToken token = restTemplate.getAccessToken();

			if (token != null) {
				expireDate = token.getExpiration();
				accessToken = token.getValue();
			}
		}

		return accessToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

}

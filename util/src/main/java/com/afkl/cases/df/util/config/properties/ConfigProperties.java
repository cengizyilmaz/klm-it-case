package com.afkl.cases.df.util.config.properties;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class is the configuration properties for general properties. klm prefix
 * in the application.properties are gathered and provided from class.
 * 
 * @author Cengiz YILMAZ
 *
 */
@Configuration
@ConfigurationProperties(prefix = "klm")
public class ConfigProperties {
	private String url;
	private String airportsList;
	private String fareCalculate;
	private Integer collectorPort;
	private String collectorHost;
	private MetricsProperties metrics = new MetricsProperties();
	private Map<String, String> customProperties = new HashMap<>();

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String> getCustomProperties() {
		return customProperties;
	}

	public String getCustomProperty(String item) {
		if (customProperties.containsKey(item)) {
			return customProperties.get(item);
		}
		return null;
	}

	public String getAirportsList() {
		return airportsList;
	}

	public void setAirportsList(String airportsList) {
		this.airportsList = airportsList;
	}

	public String getFareCalculate() {
		return fareCalculate;
	}

	public void setFareCalculate(String fareCalculate) {
		this.fareCalculate = fareCalculate;
	}

	public void setCustomProperties(Map<String, String> customProperties) {
		this.customProperties = customProperties;
	}

	public String getCollectorHost() {
		return collectorHost;
	}

	public void setCollectorHost(String collectorHost) {
		this.collectorHost = collectorHost;
	}

	public Integer getCollectorPort() {
		return collectorPort;
	}

	public void setCollectorPort(Integer collectorPort) {
		this.collectorPort = collectorPort;
	}

	public MetricsProperties getMetrics() {
		return metrics;
	}

	public void setMetrics(MetricsProperties metrics) {
		this.metrics = metrics;
	}

}

package com.afkl.cases.df.util.metrics;

/**
 * This interface is used for generalization for Metrics.
 * 
 * @author Cengiz YILMAZ
 *
 */
public interface MetricGroup {

	public String getName();

	public void merge(MetricGroup mergedGroup);

	public void assignGroup(MetricCollector collector);

}

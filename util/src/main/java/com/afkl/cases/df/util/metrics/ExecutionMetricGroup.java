package com.afkl.cases.df.util.metrics;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the generalization class of Metric Group for Execution Metric. Metric
 * value stored in the list.
 * 
 * @author Cengiz YILMAZ
 *
 */
public class ExecutionMetricGroup implements MetricGroup {
	private List<Long> metrics = new LinkedList<>();

	private String name = MetricGroupType.EXECUTIONMETRIC.name();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getMetrics() {
		return metrics;
	}

	public void setMetrics(List<Long> metrics) {
		this.metrics = metrics;
	}

	public void putMetric(Long value) {
		metrics.add(value);
	}

	@Override
	public void merge(MetricGroup mergedGroup) {
		ExecutionMetricGroup executionMetric = (ExecutionMetricGroup) mergedGroup;

		metrics.addAll(executionMetric.getMetrics());
	}

	@Override
	public void assignGroup(MetricCollector collector) {
		collector.setExecutionMetricGroup(this);

	}

}

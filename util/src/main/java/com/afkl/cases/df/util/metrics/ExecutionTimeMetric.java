package com.afkl.cases.df.util.metrics;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This is the annotation class for getting the execution metric. The annotation
 * is used above the method.
 * 
 * @author Cengiz YILMAZ
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface ExecutionTimeMetric {

}

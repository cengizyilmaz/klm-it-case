package com.afkl.cases.df.util.metrics;

/**
 * This enumaration is defined the Metric types; there are two metric types:
 * ExecutionMetric and HttpStatus Metric
 * 
 * @author Cengiz YILMAZ
 *
 */
public enum MetricGroupType {
	EXECUTIONMETRIC("ExecutionMetric"), HTTPSTATUS("HttpStatus");

	private String value;

	MetricGroupType(final String _value) {
		value = _value;
	}

	public String getValue() {
		return value;
	}

}

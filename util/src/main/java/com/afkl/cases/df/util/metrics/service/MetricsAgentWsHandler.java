package com.afkl.cases.df.util.metrics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket handler for Metric Collection for Airports and
 * Fare Services. This handler is to observe the websocket state. Connection
 * established, closed and message handling are handled by this class.The
 * /collect endpoint handled in this handler for the Airports and Fare Services
 * as agent.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class MetricsAgentWsHandler extends TextWebSocketHandler {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private MetricsAgentWsService metricsWsService;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionManager.addSession(session, session.getId(), this.getClass());
		System.out.println("Metric Collector Client Session is established. Session id " + session.getId());
		super.afterConnectionEstablished(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessionManager.removeSession(session.getId(), this.getClass());
		System.out.println("Metric Collector client session is closed.Session Id:" + session.getId() + " Close Status: "+status.getReason()+" Close Status Code: "+status.getCode());
		super.afterConnectionClosed(session, status);
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) {
		System.out.println("Transport error occurs in Metric Collector Client");
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

		metricsWsService.collectMetric(session.getId());
		super.handleMessage(session, message);
	}
}

package com.afkl.cases.df.util.metrics.service;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.afkl.cases.df.util.metrics.HttpStatusMetricGroup;
import com.afkl.cases.df.util.metrics.MetricCollector;
import com.afkl.cases.df.util.sessionmanager.SessionManager;

/**
 * This class is the WebSocket Metric Agent service.This websocket service class
 * is used to operation from websocket handler and return message through if
 * necessary. The metric collection is send to metric service from Airports and
 * Fare Services.
 *
 * @author Cengiz YILMAZ
 * 
 */
@Component
public class MetricsAgentWsService {
	@Autowired
	private SessionManager<WebSocketSession> sessionManager;
	@Autowired
	private MetricsEndpoint metricsEndPoint;
	@Autowired
	private MetricCollector collector;

	public void collectMetric(String sessionId) {

		Map<String, Object> metriclist = metricsEndPoint.invoke();
		Set<Entry<String, Object>> result = metriclist.entrySet().stream()
				.filter(item -> item.getKey().startsWith("counter.status")).collect(Collectors.toSet());
		HttpStatusMetricGroup httpStatGroup = collector.getHttpStatusMetricGroup();
		httpStatGroup.mergeMetrics(result);
		sessionManager.sendMessage(collector, sessionId, MetricsAgentWsHandler.class);

	}
}

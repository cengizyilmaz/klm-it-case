package com.afkl.cases.df.util.config.properties;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the subclass for the configProperties and provide metric properties.
 * klm.metric prefix are gathered and stored in this class.
 * 
 * @author Cengiz YILMAZ
 *
 */
public class MetricsProperties {

	private String host;
	private Integer port;
	private List<MetricsProperties> collectors = new ArrayList<>();

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public List<MetricsProperties> getCollectors() {
		return collectors;
	}

	public void setCollectors(List<MetricsProperties> collectors) {
		this.collectors = collectors;
	}

}

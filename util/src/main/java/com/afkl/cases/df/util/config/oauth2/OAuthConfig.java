package com.afkl.cases.df.util.config.oauth2;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class is the configuration properties provider for the oAuth. klm.oauth2
 * prefix in the application.properties is gathered and provided from this
 * class.
 * 
 * @author Cengiz YILMAZ
 *
 */
@Configuration
@ConfigurationProperties(prefix = "klm.oauth2")
public class OAuthConfig {

	private String accessTokenUri;

	private String clientID;

	private String clientSecret;

	public String getAccessTokenUri() {
		return accessTokenUri;
	}

	public void setAccessTokenUri(String accessTokenUri) {
		this.accessTokenUri = accessTokenUri;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

}

package com.afkl.cases.df.util.metrics;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This collector class is stored the Http Status Metric and Execution Status
 * Metric
 * 
 * @author Cengiz YILMAZ
 *
 */
@Component
public class MetricCollector {

	@JsonIgnore
	private Map<String, MetricGroup> metricGroupList = new HashMap<>();

	private HttpStatusMetricGroup httpStatusMetricGroup = populateGroup(HttpStatusMetricGroup.class);

	private ExecutionMetricGroup executionMetricGroup = populateGroup(ExecutionMetricGroup.class);

	public Map<String, MetricGroup> getMetricGroupList() {
		return metricGroupList;
	}

	public void setMetricGroupList(Map<String, MetricGroup> metricGroupList) {
		this.metricGroupList = metricGroupList;
	}

	public MetricGroup getMetricGroup(String name) {
		if (metricGroupList.containsKey(name)) {
			return metricGroupList.get(name);
		}
		return null;
	}

	public void mergeGroupList(MetricCollector collected) {

		if (metricGroupList.containsKey(collected.getExecutionMetricGroup().getName())) {
			metricGroupList.get(collected.getExecutionMetricGroup().getName())
					.merge(collected.getExecutionMetricGroup());
		} else {
			metricGroupList.put(collected.getExecutionMetricGroup().getName(), collected.getExecutionMetricGroup());
			setExecutionMetricGroup(collected.getExecutionMetricGroup());
		}

		if (metricGroupList.containsKey(collected.getHttpStatusMetricGroup().getName())) {
			metricGroupList.get(collected.getHttpStatusMetricGroup().getName())
					.merge(collected.getHttpStatusMetricGroup());
		} else {
			metricGroupList.put(collected.getHttpStatusMetricGroup().getName(), collected.getHttpStatusMetricGroup());
			setHttpStatusMetricGroup(collected.getHttpStatusMetricGroup());
		}

	}

	@SuppressWarnings("unchecked")
	private <T extends MetricGroup> T populateGroup(Class<T> metricClass) {

		T group = null;
		try {
			group = metricClass.newInstance();

		} catch (InstantiationException e) {

		} catch (IllegalAccessException e) {

		}

		if (metricGroupList.containsKey(group.getName())) {
			group = (T) metricGroupList.get(group.getName());
		}
		if (group != null) {
			metricGroupList.put(group.getName(), group);
		}

		return group;
	}

	public HttpStatusMetricGroup getHttpStatusMetricGroup() {
		return httpStatusMetricGroup;
	}

	public void setHttpStatusMetricGroup(HttpStatusMetricGroup httpStatusMetricGroup) {
		this.httpStatusMetricGroup = httpStatusMetricGroup;
	}

	public ExecutionMetricGroup getExecutionMetricGroup() {
		return executionMetricGroup;
	}

	public void setExecutionMetricGroup(ExecutionMetricGroup executionMetricGroup) {
		this.executionMetricGroup = executionMetricGroup;
	}

}

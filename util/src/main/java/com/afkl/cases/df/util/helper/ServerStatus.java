package com.afkl.cases.df.util.helper;

import java.net.ServerSocket;

/**
 * This is the utility class for the Server whether it is active or not.
 * 
 * @author Cengiz YILMAZ
 *
 */
public class ServerStatus {
	public static boolean serverListening(int port) {
		ServerSocket s = null;
		try {
			s = new ServerSocket(port);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (s != null)
				try {
					s.close();
				} catch (Exception e) {
				}
		}
	}
}

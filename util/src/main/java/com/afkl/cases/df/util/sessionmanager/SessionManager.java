package com.afkl.cases.df.util.sessionmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is the WebSocket manager to store and manage the open web socket
 * connections.
 * 
 * @author Cengiz YILMAZ
 *
 * @param <T>
 */
@Component
public class SessionManager<T extends WebSocketSession> {
	;

	private Map<Class<? extends TextWebSocketHandler>, List<T>> handlerSessions = new HashMap<>();

	public void addSession(T session, String id, Class<? extends TextWebSocketHandler> handler) {

		List<T> handlerList = null;
		if (handlerSessions.containsKey(handler)) {
			handlerList = handlerSessions.get(handler);

		} else {
			handlerList = new ArrayList<>();
		}
		if (!handlerList.contains(session)) {
			handlerList.add(session);
		}

		handlerSessions.put(handler, handlerList);

	}

	@SuppressWarnings("unlikely-arg-type")
	public void removeSession(String id, Class<? extends TextWebSocketHandler> handler) {

		handlerSessions.values().removeIf(val -> val.contains(id));
	}

	public List<T> getSession(Class<? extends TextWebSocketHandler> handler) {
		if (handlerSessions.containsKey(handler)) {
			List<T> listSession = handlerSessions.get(handler);

			return listSession;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <R> void sendMessage(R sendObject, Class<? extends TextWebSocketHandler> handler) {
		List<WebSocketSession> sessions = (List<WebSocketSession>) getSession(handler);
		if (sessions != null) {
			ObjectMapper mapper = new ObjectMapper();
			String payload;
			try {
				payload = mapper.writeValueAsString(sendObject);
				TextMessage message = new TextMessage(payload);
				sessions.stream().forEach(val -> {
					try {
						if (val.isOpen()) {
							val.sendMessage(message);
						}
					} catch (IOException e) {

						e.printStackTrace();
					}
				});

			} catch (JsonProcessingException e) {

				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <R> void sendMessage(R sendObject, String id, Class<? extends TextWebSocketHandler> handler) {
		List<WebSocketSession> sessions = (List<WebSocketSession>) getSession(handler);
		Optional<WebSocketSession> session = sessions.stream().filter(val -> val.getId().equals(id)).findFirst();
		if (session.isPresent()) {
			ObjectMapper mapper = new ObjectMapper();
			String payload;
			try {
				payload = mapper.writeValueAsString(sendObject);
				TextMessage message = new TextMessage(payload);

				session.get().sendMessage(message);

			} catch (JsonProcessingException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

}

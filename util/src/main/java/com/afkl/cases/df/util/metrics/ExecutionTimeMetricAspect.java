package com.afkl.cases.df.util.metrics;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This aspect class is used to get the execution time metric. The point cut
 * method is around the ExecutionTimeMetric annotation.
 * 
 * @author Cengiz YILMAZ
 *
 */
@Aspect
@Component
public class ExecutionTimeMetricAspect {
	@Autowired
	private MetricCollector collector;

	@Around("@annotation(ExecutionTimeMetric)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
		long start = System.currentTimeMillis();

		Object proceed = joinPoint.proceed();

		long executionTime = System.currentTimeMillis() - start;
		ExecutionMetricGroup group = collector.getExecutionMetricGroup();
		group.putMetric(executionTime);
		System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms");
		return proceed;

	}
}

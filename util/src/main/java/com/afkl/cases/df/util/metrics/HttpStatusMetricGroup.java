package com.afkl.cases.df.util.metrics;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
/**
 * This is the generalization class of Metric Group for Http Status Metric. Metric
 * value stored in the map.
 * 
 * @author Cengiz YILMAZ
 *
 */
public class HttpStatusMetricGroup implements MetricGroup {

	private Map<String, Number> statusList = new HashMap<>();
	private String name = MetricGroupType.HTTPSTATUS.name();

	@Override
	public String getName() {

		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addMetric(String name, Integer value) {
		statusList.put(name, value);
	}

	public Map<String, Number> getStatusList() {
		return statusList;
	}

	public void setStatusList(Map<String, Number> statusList) {
		this.statusList = statusList;
	}

	public void merge(MetricGroup group) {
		HttpStatusMetricGroup mergedGroup = (HttpStatusMetricGroup) group;
		mergedGroup.getStatusList().entrySet().stream().forEach(val -> {

			if (statusList.containsKey(val.getKey())) {
				statusList.put(val.getKey(), statusList.get(val.getKey()).longValue() + val.getValue().longValue());
			} else {
				statusList.put(val.getKey(), val.getValue());
			}
		});
	}

	public void mergeMetrics(Set<Entry<String, Object>> result) {
		result.stream().forEach(val -> {
			if (val.getValue() instanceof Number) {
				statusList.put(val.getKey(), (Number) val.getValue());
			}
		});

	}

	@Override
	public void assignGroup(MetricCollector collector) {
		collector.setHttpStatusMetricGroup(this);

	}

	public void reset() {
		statusList.clear();
	}

}

cd afklmdevnet-simple-travel-api-mock
start "Mock Service" cmd.exe /k "gradlew.bat bootrun"
cd ..
cd airports
start "Airport Service" cmd.exe /k "gradlew.bat bootrun"
cd ..
cd fares
start "Fare Service" cmd.exe /k "gradlew.bat bootrun"
cd ..
cd frontend
start "FrontEnd App" cmd.exe /k "gradlew.bat bootrun"
cd ..
cd metrics
start "Metrics Service" cmd.exe /k "gradlew.bat bootrun"
cd ..

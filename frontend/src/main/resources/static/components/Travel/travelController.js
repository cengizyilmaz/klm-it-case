angular.module('travel', ['ui.router', 'ui.bootstrap', 'airportsService', 'faresService', 'ui-leaflet'])


  .config(['$stateProvider', '$routeProvider', '$qProvider', function($stateProvider, $routeProvider, $qProvider) {


  }])

  .controller('TravelController', function TravelController($scope, $q, leafletData, AirportsService, FaresService) {
    angular.extend($scope, {
      center: {
        lat: 40.095,
        lng: -3.823,
        zoom: 4
      },
      defaults: {
        scrollWheelZoom: false
      }
    });
    var requestId = 0;



    $scope.getRequestId = function() {
      return requestId++;
    };

    $scope.request = function(data) {
      var request = {
        request_id: $scope.getRequestId(),
        data: data,
        lang: $scope.$parent.langModel,
        messageType: "getAirports"
      };

      var deferred = $q.defer();
      AirportsService.callbacks[request.request_id] = deferred;
      AirportsService.getAirports(angular.toJson(request));

      return deferred.promise.then(function(response) {
        var result = response.locations.map(function(item) {
          return item.code;
        });
        console.log(response);
        request.response = result;
        return response.locations;
      });
    };

    $scope.calculate = function() {
      loadProgress();
      var request = {
        request_id: $scope.getRequestId(),
        from: this.controller.from.code,
        to: this.controller.to.code
      };
      var deferred = $q.defer();
      FaresService.callbacks[request.request_id] = deferred;
      FaresService.calculate(angular.toJson(request));

      return deferred.promise.then(function(response) {

        resetUI($scope);
        $scope.fareResult = response;
        return response;
      });
    };

    $scope.mapLocation = function() {
      var from = this.controller.from;
      var to = this.controller.to;
      leafletData.getMap('klm-travel-map').then(function(map) {

        map.removeLayer(markerFrom);
        map.removeLayer(markerTo);
        map.removeLayer(polyline);
        var markerFrom = L.marker([from.coordinates.latitude, from.coordinates.longitude]).bindPopup("From: " + from.description).addTo(map);
        var markerTo = L.marker([to.coordinates.latitude, to.coordinates.longitude]).bindPopup("To: " + to.description).addTo(map);
        var pathCoords = [];
        var latlngs = [
          [from.coordinates.latitude, from.coordinates.longitude],
          [to.coordinates.latitude, to.coordinates.longitude]

        ];
        var polyline = L.polyline(latlngs, {
          color: 'red'
        }).addTo(map);

        // zoom the map to the polyline
        map.fitBounds(polyline.getBounds());


      });


    }

    function resetUI($scope) {
      $("#calculateBtn").button('reset');
      $("#calculateBtn").removeAttr('disabled');
      $("#toInput").button('reset');
      $("#toInput").removeAttr('disabled');
      $("#fromInput").button('reset');
      $("#fromInput").removeAttr('disabled');

      $('#fareResult').removeAttr('hidden');
    };

    function loadProgress() {
      $("#calculateBtn").attr('disabled', 'disabled');
      $("#toInput").attr('disabled', 'disabled');
      $("#fromInput").attr('disabled', 'disabled');;

      $("#calculateBtn").button('loading');
    };

  })

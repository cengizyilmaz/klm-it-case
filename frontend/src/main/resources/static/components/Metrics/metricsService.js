angular.module('metricsService', ['angular-websocket'])
.service('MetricsService',['$http','$timeout','$log', '$websocket',MetricsService]);
function MetricsService($http,$timeout,$log,$websocket)
{
  var service = {};

  service.callbacks = {};
  service.response;
  service.getMetrics=getMetrics;
     var baseElement = document.getElementsByTagName('base')[0];
     var baseUrl = baseElement ? baseElement.getAttribute('href') : '/';
     service.ws = $websocket('ws://' + location.hostname + ':' + 7777 + baseUrl + 'display');

     service.ws.onMessage(function(message){
       var data = angular.fromJson(message.data);
       service.response=data;
     });

  return service;
  function getMetrics(request)
  {
    service.ws.send(request);
  }
}

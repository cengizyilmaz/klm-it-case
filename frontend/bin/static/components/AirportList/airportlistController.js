angular.module('airportlist',['ui.router','ui.bootstrap','airportsService','ngTable'])


.config([ '$stateProvider','$routeProvider','$qProvider',function ($stateProvider,$routeProvider,$qProvider) {


}
])

.controller('AirportListController',   function AirportListController($scope,$q,AirportsService,NgTableParams)
{
  var requestId = 0;



$scope.getRequestId = function() {
  return requestId++;
};

this.airportlistTable = new NgTableParams({ count: 10 }, {
      getData: function(params) {
        var url=params.url();
  var request = {
    request_id: $scope.getRequestId(),
    page:url.page,
    count:url.count,
    lang:$scope.$parent.langModel,
    messageType:"listAirports"
  };

  var deferred = $q.defer();
  AirportsService.callbacks[request.request_id] = deferred;
  AirportsService.getAirports(angular.toJson(request));

  return deferred.promise.then(function(response) {
    params.total(1048);
    return response.locations;
  });
}
});

}
);

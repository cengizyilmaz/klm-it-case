angular.module('faresService', ['angular-websocket'])
.service('FaresService',['$http','$timeout','$log', '$websocket',FaresService]);
function FaresService($http,$timeout,$log,$websocket)
{

   var service = {};

   service.callbacks = {};
   service.calculate=calculate;
      var baseElement = document.getElementsByTagName('base')[0];
      var baseUrl = baseElement ? baseElement.getAttribute('href') : '/';
      service.ws = $websocket('ws://' + location.hostname + ':' + 9999 + baseUrl + 'fares');

      service.ws.onMessage(function(message){
        var data = angular.fromJson(message.data);
          if (angular.isDefined(service.callbacks[data.request_id])) {
            var callback = service.callbacks[data.request_id];
            delete service.callbacks[data.request_id];
            callback.resolve(data);
          } else {
            $log.error("Unhandled message: %o", data);
          }
      });

   return service;

   function calculate(request)
   {

    service.ws.send(request);
   }


}

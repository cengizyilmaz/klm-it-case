angular.module('airportsService', ['angular-websocket'])
.service('AirportsService',['$http','$timeout','$log','$websocket',AirportsService]);
function AirportsService($http,$timeout,$log,$websocket)
{

   var service = {};

   service.callbacks = {};
   service.getAirports=getAirports;
   service.getLocation=getLocation;
      var baseElement = document.getElementsByTagName('base')[0];
      var baseUrl = baseElement ? baseElement.getAttribute('href') : '/';
      service.ws = $websocket('ws://' + location.hostname + ':' + 8888 + baseUrl + 'airports');

      service.ws.onMessage(function(message){
        var data = angular.fromJson(message.data);
          if (angular.isDefined(service.callbacks[data.request_id])) {
            var callback = service.callbacks[data.request_id];
            delete service.callbacks[data.request_id];
            callback.resolve(data);
          } else {
            $log.error("Unhandled message: %o", data);
          }

      });

   return service;

   function getAirports(request)
   {
     service.ws.send(request);
   }

   function getLocation(request)
   {
     service.ws.send(request);
   }

}

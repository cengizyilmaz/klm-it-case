angular.module('metrics',['ui.router','ui.bootstrap','chart.js','metricsService'])


.config([ '$stateProvider','$routeProvider','$qProvider',function ($stateProvider,$routeProvider,$qProvider) {


}
])

.controller('MetricsController',  function MetricsController($scope,$q,MetricsService)
{

  $scope.collectMetric = function() {
    var request = {
      request_id: 0,

    };
 MetricsService.getMetrics(angular.toJson(request));


};
$scope.$watch( function () { return MetricsService.response; }, function ( response ) {
  $scope.labels=response.labels;
    $scope.data=response.data;
    $scope.average=response.average;
    $scope.max=response.max;
    $scope.min=response.min;
	});
});

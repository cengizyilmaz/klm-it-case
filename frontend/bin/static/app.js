'use strict';
angular.module('app', [
 'ngRoute','ui.bootstrap','travel','metrics','airportlist']);



angular.module('app').config([ '$stateProvider','$routeProvider', '$locationProvider', function ( $stateProvider,$routeProvider,$locationProvider) {
 $routeProvider
 .when('/travel', {
   templateUrl:'/components/Travel/travel.tpl.html',
   controller:'TravelController'
 })
.when('/metrics',{
   templateUrl:'/components/Metrics/metrics.tpl.html',
   controller:'MetricsController'
 })
 .when('/airportlist',{
    templateUrl:'/components/AirportList/airportlist.tpl.html',
    controller:'AirportListController'
  })
 ;
 $locationProvider.html5Mode({
enabled: true,
requireBase: true
});
 $routeProvider.otherwise({redirectTo:'/travel'});
}]);

angular.module('app').controller('AppCtrl', ['$scope', function($scope) {

  $scope.langModel = 'en';
}]);
